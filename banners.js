// метод setTimeout нужен для установления задержки перед выполнением функции (вызов функции один раз)
// метод setInterval предназначен для установления интервала между выполнением функции (вызрв функции регулярно)
//
// setTimeout(() => {...}, 0) нужен для как можно быстрого вызова функции, но и для макрозадач так как имеет свойство 
// выполнять действие внутри себя только после выполненого обычного кода, таким способом мы можем устанавливать "очереди" 
// на выполнение тех или иных задач, так как ресурсоёмкие операции требуют первоначального выполнения их кода, а затем уже 
// каких-то событий, с помощью setTimeout с таймаутом 0 мы можем разделять выполнения сложной задачи для промежуточных 
// выполнений других задач
// 
// когда ранее созданный цикл запуска вам уже не нужен clearInterval() стоит вызывать для остановки бесконечного интервала
// иначе он собственно продлится до того как мы не обновим страничку
//
const images = document.querySelectorAll(".image-to-show")
let currentImg = 0
flag = false

const timerSec = document.querySelector('.timer1');
const timerMls = document.querySelector('.timer2');

let sec = 2
let mls = 100    

const setSecondsTimer = function () {
    timerSec.innerHTML = `until next image: ${sec} seconds`
    sec--
    sec < 0 ? sec = 2 : sec
}
setSecondsTimer()

const setMlsTimer = function () {
    timerMls.innerHTML = `${mls} miliseconds`
    mls--
    mls === 0 ? mls = 100 : mls
}
setMlsTimer()


let secInterval = setInterval(setSecondsTimer, 1000)
let mlsInterval = setInterval(setMlsTimer, 10)

const showNextImage = function () {
    let prevImg = currentImg - 1
    currentImg === 0 ? prevImg = images.length - 1 : prevImg
    images[prevImg].classList.add("fade")
    
    setTimeout(() => {
        images[prevImg].classList.remove("active")
        images[currentImg].classList.add("active")
    }, 500)

    setTimeout(() => {
        images[currentImg].classList.remove("fade")
        currentImg++ 
    }, 550)

    currentImg >= Object.keys(images).length ? currentImg = 0 : false
}

showNextImage()

let interval = setInterval(showNextImage, 3000);

document.querySelector(".stopBtn").addEventListener("click", () => {
    if (!flag) {
        flag = true
        clearInterval(secInterval);
        clearInterval(mlsInterval);
        clearInterval(interval)
    }
}) 
document.querySelector(".resetBtn").addEventListener("click", () => {
    if (flag) {
        flag = false
        sec = 2;
        mls = 100;
        setSecondsTimer()
        setMlsTimer()
        secInterval = setInterval(setSecondsTimer, 1000);
        mlsInterval = setInterval(setMlsTimer, 10);
        interval = setInterval(showNextImage, 3000);
    }
})